# Deciphering non verbal behaviours

### Jupyter notebooks:

[Version 0](https://colab.research.google.com/drive/1Ct4q2mRVBq388Duier8hc3nGZm5XT5ZG?usp=sharing)

[Version 1](https://colab.research.google.com/drive/1oqIoh73WEgPQ0vRVj955mKgK_DJimfjI#scrollTo=0JYi2Ca0hbHr)

[Version 2](https://colab.research.google.com/drive/13LEnSzpID61hKMQHNyKXlRLeMdSUBKJm?usp=sharing#scrollTo=uzvBW6sLYXWt)
